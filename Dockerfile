FROM tdfs/tdfs_docker_php74:latest

# RUN apk --no-cache update && apk --no-cache upgrade && apk add --no-cache bash

WORKDIR /var/www/html

COPY ./ /var/www/html

COPY ./php.ini /usr/local/etc/php/conf.d/my-custom-php.ini

#Supervisor
# COPY ./worker.conf /etc/supervisor/conf.d/worker.conf

EXPOSE 8085

RUN chmod -R 777 storage/
# RUN composer install
# RUN npm install

CMD bash -c "cd /var/www/html && cp .env.dev .env && composer install  && php artisan config:cache && php artisan storage:link && php artisan serve --host=0.0.0.0 --port=8085"

#run cmd supervisor and schedule
# service supervisor start worker:*
# php artisan schedule:work