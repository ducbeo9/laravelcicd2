# before_script:
#   - docker info
# - docker network create -d bridge tdfs_story_live

# stages:
#   - build-production
#   - production
#   - queue-jobs

# Build Production:
#   stage: build-production
#   script:
#     - echo "Build"
#     - set -ex
#     - TAG=$(git describe --abbrev=0 --tags)
#     - NAME_IMAGE=laravelcicd
#     - REGISTRY_HUB=registry.gitlab.com/ducbeo9/laravelcicd2
#     - docker login registry.gitlab.com -u ducbeo9 -p glpat-TkAmsEaXwdGVyXe1HTe4
#     - docker image build --progress=plain -t $REGISTRY_HUB/live_$NAME_IMAGE:$TAG -t $REGISTRY_HUB/live_$NAME_IMAGE:latest -f ./Dockerfile .
#     - docker image push $REGISTRY_HUB/live_$NAME_IMAGE:$TAG
#     - docker image push $REGISTRY_HUB/live_$NAME_IMAGE:latest
#   only:
#     - /^live_[0-9]+(?:.[0-9]+)+$/ # regular expression

# Deploy to Production:
#   stage: production
#   script:
#     - echo "Deployed"
#     - set -ex
#     - TAG=$(git describe --abbrev=0 --tags)
#     - NAME_IMAGE=laravelcicd
#     - REGISTRY_HUB=registry.gitlab.com/ducbeo9/laravelcicd2
#     - docker login registry.gitlab.com -u ducbeo9 -p glpat-TkAmsEaXwdGVyXe1HTe4
#     - bash stop_docker.sh
#     - docker run -d --name "tdfs_story_live" --network="tdfs_story_live" -v tdfs_story_storage:/var/www/html/storage --restart=always -p 8085:8085 $REGISTRY_HUB/live_$NAME_IMAGE:$TAG
#   only:
#     - /^live_[0-9]+(?:.[0-9]+)+$/ # regular expression

# Deploy to Queue-jobs:
#   stage: queue-jobs
#   script:
#     - echo "Queue - Jobs"
#     # - bash run_worker.sh
#     # - bash run_job.sh
#   # when: delayed
#   # start_in: 2 minutes
#   only:
#     - /^live_[0-9]+(?:.[0-9]+)+$/ # regular expression